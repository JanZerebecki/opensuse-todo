Possible OpenSUSE improvements
##############################

- Installer should support wifi scanning, currently one needs to manually input
  the ESSID (network name).
- Installing with full disk encryption can only be found in the expert
  installation. Shouldn't this be an option or even the default in the
  non-expert one?
- Is it obvious enough in the installer that LVM is not needed, because of
  btrfs? (Didn't yet test if the installer offers LVM with ext4.)
- Installer should explain what the encrypted option enables. That it is full
  disk without an unencrypted boot partition? That it is provided by
  cryptsetup?
- Booting should only ask once for the full disk encryption password. Instead
  of twice, once for Grub and once for Linux.
- Installer should install updated packages directly instead of only the
  updater after the first boot. There were >400 packages to update on a Leap
  15.1 install.
- `zypper in mate` should find and install the pattern, but doesn't find
  anything. `zypper in -t pattern mate` does.

